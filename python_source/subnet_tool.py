# Copyright 2022 corycapps.com
# Released under GPL-2.0 License
# Please feel free to tinker, hack and explore.

def ip2dec(network):
  netlist=network.split(".")
  ipdec = ( (int(netlist[0]) << 24) + (int(netlist[1]) << 16) + (int(netlist[2]) << 8) + int(netlist[3]) )
  return ipdec

def dec2ip(netdec):
  ip='.'.join([str(netdec >> (i << 3) & 0xFF) for i in range(4)[::-1]])
  return ip

def subnetsplit(subnet):
  return subnet.split(".")

def dec2bin(dec):
  return bin(int(dec)).split("0b")[1]

def bin2dec(vbin):
  vbin="0b"+str(vbin)
  return int(vbin, 2)

cidrorsub=input("CIDR Mode(y/n): ")
if(cidrorsub == "y"):
  cidr=input("Enter CIDR: ")
  subnetbin=""
  loops=0
  while (int(loops)<int(cidr)):
    subnetbin=str(subnetbin)+"1" # Toss a 1 in for each cidr int
    loops=loops+1
  while (int(loops)<32):
    subnetbin=str(subnetbin)+"0" # Fill in the rest with 0's until we are 32 chrs long
    loops=loops+1
  subnetdec=bin2dec(subnetbin) # Convert bin string to dec
  print("Subnet Binary: ",subnetbin)
  subnet=dec2ip(subnetdec) # Convert dec to ip, in this case subnet since the math is the same
  print("Subnet: ",subnet)
else:
  subnetin=input("Enter Subnet: ")
  subnetbin=dec2bin(ip2dec(subnetin))
  print("Subnet Binary: ",subnetbin)
  cidr=0
  for bit in list(subnetbin):
    if (int(bit) == 1):
      cidr=int(cidr)+1
  print("CIDR: ",cidr)
# Print Subnet Info
urexp=32-int(cidr) # Setup Exponet
urf=2**urexp # 2^32-CIDR 
ur=urf-2 # Sub 2 for broadcast and network
print("Useable Range: ",ur)
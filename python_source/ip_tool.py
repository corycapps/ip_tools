# Copyright 2022 corycapps.com
# Released under GPL-2.0 License
# Please feel free to tinker, hack and explore.
def ip2dec(network):
  netlist=network.split(".")
  ipdec = ( (int(netlist[0]) << 24) + (int(netlist[1]) << 16) + (int(netlist[2]) << 8) + int(netlist[3]) )
  return ipdec

def dec2ip(netdec):
  ip='.'.join([str(netdec >> (i << 3) & 0xFF) for i in range(4)[::-1]])
  return ip

def subnetsplit(subnet):
  return subnet.split(".")

def dec2bin(dec):
  return bin(int(dec)).split("0b")[1]

def bin2dec(vbin):
  vbin="0b"+str(vbin)
  return int(vbin, 2)

network=input("Enter Network: ")
subnettypesel=input("CIDR or Netmask (c/n)?: ")

if ( str(subnettypesel) == "c" ):
  cidr=input("Enter CIDR: ")
  subnetbin=""
  loops=0
  while (int(loops)<int(cidr)):
    subnetbin=str(subnetbin)+"1" # Toss a 1 in for each cidr int
    loops=loops+1
  while (int(loops)<32):
    subnetbin=str(subnetbin)+"0" # Fill in the rest with 0's until we are 32 chrs long
    loops=loops+1
  subnetdec=bin2dec(subnetbin) # Convert bin string to dec
  subnet=dec2ip(subnetdec) # Convert dec to ip, in this case subnet since the math is the same

else:
  subnet=input("Enter Subnet: ")
  subnetbin=dec2bin(ip2dec(subnet))
  cidr=0
  for bit in list(subnetbin):
    if (int(bit) == 1):
      cidr=int(cidr)+1 # Count each 1 in the subnet, lazy compute the CIDR number.

# After all of that we now know
# Network, Mask and CIDR

# Calculate Usable Range
urexp=32-int(cidr) # Setup Exponet
urf=2**urexp # 2^32-CIDR 
useablerange=urf-2 # Sub 2 for broadcast and network

# Build Broadcast Address String
if((ip2dec(network)+int(urf)-1) > (2**32)): # Max number of addresses in ipv4 is 2^32nd
      broadcast="Invalid IP: Out of Bound"
else: 
  broadcast=dec2ip(ip2dec(network)+int(urf)-1)
  if(int(cidr) != 32):
    firstuseable=dec2ip(ip2dec(network)+1)
  else: # IF CIDR address is 32 then the first address is == to the broadcast and first usable
    firstuseable=network

# Single Address Message
if(int(useablerange) < 0):
  useablerange="SingleIPMode"

# Output
print("======================")
print("     NETWORK INFO     ")
print("======================")
print("Network Address: ",network)
print("CIDR: ",cidr)
print("Netmask: ",subnet)
print("Network Address: ",network)
print("First Usable Address: ",firstuseable)
print("Broadcast Address: ",broadcast)
print("Usable IPs: ",useablerange)


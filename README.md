# TI-Nspire IP Network Tools (Python)

### What is this project

A product of boredom and noticing there was a lack of simple python programs for the nspire to calculate subnets and usable ip's / broadcast ips. I really hope that this will help kids in school peer behind the numbers of IP addressing. 

### How to use

1. Load the pre-built .tns onto your nspire calculator using the student or teacher software
1. On the calculator use the browse menu from the home screen
1. Browse to the directory where you dropped the TNS file on your device
1. Once you have opened the document use CTRL+R on your calculator to run the script.

### Screenshots

ip_tool:
![ip tool screenshot](./screenshots/iptool_screenshot.png)

subnet_tool:
![subnet tool screenshot](./screenshots/subnettool_screenshot.png)

### License 

GPL-2.0